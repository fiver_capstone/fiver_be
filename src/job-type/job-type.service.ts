import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateJobTypeDto } from './dto/create-job-type.dto';
import { UpdateJobTypeDto } from './dto/update-job-type.dto';
import { PrismaClient, type_job } from '@prisma/client';
import { ResponseData } from 'src/global/globalClass';
import { HttpMessage } from 'src/global/globalEnum';

@Injectable()
export class JobTypeService {
  prisma = new PrismaClient();

  async createJobType(
    createJobTypeDto: CreateJobTypeDto,
  ): Promise<CreateJobTypeDto> {
    const newJobType = await this.prisma.type_job.create({
      data: createJobTypeDto,
    });
    return newJobType;
  }

  async findJobType(page: number, limit: number) {
    return await this.prisma.type_job.findMany({
      skip: (page - 1) * limit,
      take: limit,
    });
  }

  findJobTypeById(id: number) {
    const jobType = this.prisma.type_job.findFirst({
      where: {
        id,
      },
    });
    return jobType;
  }

  updateJobType(id: number, updateJobTypeDto: UpdateJobTypeDto) {
    const jobType = this.prisma.type_job.findFirst({
      where: {
        id,
      },
    });
    if (jobType) {
      const jobTypeUpdate = this.prisma.type_job.update({
        where: { id },
        data: updateJobTypeDto,
      });
      return jobTypeUpdate;
    } else {
      throw new NotFoundException(HttpMessage.NOT_FOUND);
    }
  }

  async removeJobType(id: number) {
    await this.prisma.type_job.delete({
      where: {
        id,
      },
    });
    return;
  }
}
