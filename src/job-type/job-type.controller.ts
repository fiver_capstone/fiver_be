import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
} from '@nestjs/common';
import { JobTypeService } from './job-type.service';
import { CreateJobTypeDto } from './dto/create-job-type.dto';
import { UpdateJobTypeDto } from './dto/update-job-type.dto';
import { type_job } from '@prisma/client';
import { errorCode } from 'src/configs/response';
import { ResponseData } from 'src/global/globalClass';
import { HttpMessage, HttpStatus } from 'src/global/globalEnum';

@Controller('job-type')
export class JobTypeController {
  constructor(private readonly jobTypeService: JobTypeService) {}

  @Post('/create-job-type')
  async create(
    @Body() createJobTypeDto: CreateJobTypeDto,
  ): Promise<ResponseData<CreateJobTypeDto>> {
    try {
      const newJobType: CreateJobTypeDto =
        await this.jobTypeService.createJobType(createJobTypeDto);

      return new ResponseData<CreateJobTypeDto>(
        newJobType,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/get-job-type')
  async findJobType(
    @Body('page') page: number,
    @Body('limit') limit: number,
  ): Promise<ResponseData<type_job[]>> {
    try {
      if (!page) {
        page = 1;
      }

      if (!limit) {
        limit = 1000;
      } else {
        limit = Math.min(5, limit);
      }
      const jobType = await this.jobTypeService.findJobType(page, limit);
      return new ResponseData<type_job[]>(
        jobType,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/get-job-type-by-id/:id')
  async findJobTypeById(
    @Param('id') id: string,
  ): Promise<ResponseData<type_job>> {
    try {
      const jobType = await this.jobTypeService.findJobTypeById(+id);
      return new ResponseData<type_job>(
        jobType,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch('update-job-type/:id')
  async update(
    @Param('id') id: string,
    @Body() updateJobTypeDto: UpdateJobTypeDto,
  ): Promise<ResponseData<UpdateJobTypeDto>> {
    try {
      const jobTypeUpdate = await this.jobTypeService.updateJobType(
        +id,
        updateJobTypeDto,
      );
      return new ResponseData<UpdateJobTypeDto>(
        jobTypeUpdate,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete('delete-job-type/:id')
  async removeJobType(
    @Param('id') id: string,
  ): Promise<ResponseData<type_job>> {
    try {
      await this.jobTypeService.removeJobType(+id);
      return new ResponseData<type_job>(
        null,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
