import { IsEmpty, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateJobTypeDto {
  @IsEmpty()
  @IsNumber()
  id: number;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  name_job_type: string;
}
