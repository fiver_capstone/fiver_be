import { Controller, Get, Post, Body, Patch, Param, Delete,Headers, UseGuards, Req, Res } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { comment } from '@prisma/client';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}
  @UseGuards(AuthGuard("jwt"))
  @Post('create-comment')
  create(@Body() createCommentDto: CreateCommentDto,@Req() req, @Res() res:Response ) {
    const userId = parseInt(req.user.user_id)
    return this.commentsService.createComment(createCommentDto,userId,res);
  }
  
  @Get('get-all-comments')
  findAll(@Res() res){
    return this.commentsService.getAllComments(res);
  }

  @Get('get-comments-to-work/:id')
  getCommentsRelatedToWork(@Param('id') id: string ,@Res() res) {
    return this.commentsService.getCommentsRelatedToWork(+id,res);
  }

  @UseGuards(AuthGuard("jwt"))
  @Patch('edit-comment/:id')
  update( @Param('id') id: string,@Body() updateCommentDto: UpdateCommentDto,@Req() req, @Res() res:Response) {
    const user_id = parseInt(req.user.user_id)
    return this.commentsService.updateComment(updateCommentDto,+user_id,res,+id);
  }
  @UseGuards(AuthGuard("jwt"))
  @Delete('delete-comment/:id')
  deleteComment(@Param('id') id: string,@Req() req,@Res() res:Response, ) {
    const user_id = parseInt(req.user.user_id)
    return this.commentsService.deleteComment(+id,user_id,res);
  }
}
