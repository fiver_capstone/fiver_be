import { Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { PrismaClient, comment } from '@prisma/client';
import { failCode, successCode } from 'src/configs/response';
import { Response } from 'express';
@Injectable()
export class CommentsService {
  prisma = new PrismaClient()
  async createComment(createCommentDto: CreateCommentDto,userId:number,res:Response) {    
    const jobId =createCommentDto.job_id
    const foundJobId = await this.prisma.job.findUnique({
      where: {
        id:jobId,
      }
    })
    if(foundJobId){

      const date_comment = new Date().toISOString();
      let newData = {
        user: { connect: { id: userId } },
        job: { connect: { id: foundJobId.id } },
        content: createCommentDto.content,
        date_comment,
        star_comment: createCommentDto.star_comment
      };
      
      if(newData){
        
        let addComment = await this.prisma.comment.create({data: newData})
        if(addComment){
          successCode(res,addComment,"Success Added")
        }else{
          failCode(res,"","Failed to add")
        }
      }
    }
    
    
  }

 async getAllComments(res:Response) {
  let data = await this.prisma.comment.findMany()
  if(data){
    successCode(res,data,"Success Loaded Comments")
  }
  }

  async getCommentsRelatedToWork(id: number,res:Response) {
    const findCommentsRelated = await this.prisma.job.findMany({
      where:{
        id
      },
      include:{comment:true}
    })
    if(findCommentsRelated.length>0){
      successCode(res,findCommentsRelated,"Found comments")
      return findCommentsRelated
    }else{
      failCode(res,"","Found no comments")
    }
  }


  async updateComment(updateCommentDto: UpdateCommentDto, user_id: number, res: Response,id:number) {
    let foundComment = await this.prisma.comment.findUnique({
        where: {
            id,
        }
    });

    if (!foundComment) {
        return failCode(res, "", "Comment not found");
    }

    // Ensure that the user trying to update is the author of the comment
    if (foundComment.user_id !== user_id) {
        return failCode(res, "", "You are not the author of this comment");
    }

    const { job_id,...otherData } = updateCommentDto;
    const date_comment = new Date().toISOString();
    let newData = {
        ...otherData, 
        date_comment,
    };

    if (updateCommentDto.star_comment) {
        newData.star_comment = +updateCommentDto.star_comment;
    }

    let updatedComment = await this.prisma.comment.update({
        where: { id },
        data: newData
    });

    if (updatedComment) {
        successCode(res, updateCommentDto, "Success Updated");
    } else {
        failCode(res, "", "Failed to update");
    }

    return `This action updates a #${id} comment`;
}


 async deleteComment(id: number,user_id:number,res:Response) {
    const checkAuthor = await this.prisma.comment.findFirst({
      where: {
        id,
        user_id
      }
    })
    if(checkAuthor){
      const deleted = await this.prisma.comment.delete({
        where:{
          id: checkAuthor.id
        }
      })
      successCode(res,deleted,"Deleted comment")
    }else{
      failCode(res,"","You don't have permission to delete this comment")
    }
   
  }
}
