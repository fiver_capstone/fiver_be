import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Res,
  Query,
  UseInterceptors,
  UploadedFile
} from '@nestjs/common';
import { JobService } from './job.service';
import { UpdateJobDto } from './dto/update-job.dto';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { PaginationDto } from './dto/paginated-job.dto';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiResponse, ApiTags, ApiParam, ApiQuery, ApiConsumes } from '@nestjs/swagger';
import { CreateJobDto } from './dto/create-job.dto';

@ApiTags('Job')
@Controller('job')
export class JobController {
  constructor(private readonly jobService: JobService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('/create-new-job')
  @UseInterceptors(FileInterceptor('image'))
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: 'Create a new job' })
  @ApiBearerAuth()
  @ApiBody({
    description: 'Job Data',
    type: 'multipart/form-data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        image: { type: 'string', format: 'binary', description: 'File to upload', },
        job_name: {type:"string"},
        rating: {type:"number"},
        price: {type:"number"},
        star_job: {type:"number"},
        type_job: {type:"number"},
        description: {type:"string"},
        short_description: {type:"string"},
      },
      required: [
        "image", 
        "job_name", 
        "rating", 
        "price", 
        "star_job", 
        "type_job_id", 
        "description", 
        "short_description"
      ]
    },
  })
  @ApiResponse({ status: 201, description: 'Job successfully created.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  async createNewJob(
      @UploadedFile() file: Express.Multer.File,
      @Body() body:CreateJobDto,
      @Req() req,
      @Res() res
  ) {
      const userId = req.user.userId;
      return this.jobService.createNewJob(body, userId, file, res);
  }

  @Get('/get-all-job')
  @ApiOperation({ summary: 'Retrieve all jobs' })
  @ApiResponse({ status: 200, description: 'List of all jobs' })
  getAllJob(@Res() res) {
      return this.jobService.getAllJob(res);
  }

  @Get('/get-job/:id')
  @ApiOperation({ summary: 'Retrieve a specific job by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job to retrieve' })
  @ApiResponse({ status: 200, description: 'Job details' })
  getOneJob(@Param('id') id: string, @Res() res) {
      return this.jobService.getOneJob(+id, res);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('/edit/:id')
  @UseInterceptors(FileInterceptor('image')) 
  @ApiOperation({ summary: 'Update a job by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job to update' })
  @ApiBearerAuth()
  @ApiBody({ type: UpdateJobDto, description: 'Updated Job Data' })
  @ApiResponse({ status: 200, description: 'Job updated successfully.' })
  updateJob(@UploadedFile() file: Express.Multer.File, @Param('id') id: string, @Body() body, @Req() req, @Res() res) {
      const userId = req.user.userId;
      return this.jobService.updateJob(+id, body, file, userId, res);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/delete/:id')
  @ApiOperation({ summary: 'Delete a job by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job to delete' })
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Job deleted successfully.' })
  deleteJob(@Param('id') id: string, @Req() req, @Res() res) {
      const userId = req.user.userId;
      return this.jobService.deleteJob(+id, userId, res);
  }

  @Get('/menu')
  @ApiOperation({ summary: 'Retrieve job menu' })
  @ApiResponse({ status: 200, description: 'Job menu' })
  getMenu(@Res() res) {
      return this.jobService.getMenu(res);
  }

  @Get('/get-detail-type-job-type-job/:id')
  @ApiOperation({ summary: 'Get job detail type for a specific job type' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job type' })
  @ApiResponse({ status: 200, description: 'Job detail type for the specific job type' })
  getDetailTypeJobToTypeJob(@Param('id') id: string, @Res() res) {
      return this.jobService.getDetailTypeJobToTypeJob(+id, res);
  }

  @Get('/get-job-detail-type-job/:id')
  @ApiOperation({ summary: 'Get jobs of a specific detail type' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job detail type' })
  @ApiResponse({ status: 200, description: 'Jobs of the specific detail type' })
  getJobToDetailTypeJob(@Param('id') id: string, @Res() res) {
      return this.jobService.getJobToDetailTypeJob(+id, res);
  }

  @Get('/get-detail-type-job/:id')
  @ApiOperation({ summary: 'Get job detail type by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job detail type' })
  @ApiResponse({ status: 200, description: 'Job detail type' })
  geDetailTypeJob(@Param('id') id: string, @Res() res) {
      return this.jobService.getDetailTypeJob(+id, res);
  }

  @Get('/get-detail-job/:id')
  @ApiOperation({ summary: 'Get job details by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job' })
  @ApiResponse({ status: 200, description: 'Job details' })
  getDetailJob(@Param('id') id: string, @Res() res) {
      return this.jobService.getDetailJob(+id, res);
  }

  @Get('/get-list-jobs-from-name/:name')
  @ApiOperation({ summary: 'Get jobs by name' })
  @ApiParam({ name: 'name', type: 'string', description: 'Name of the job' })
  @ApiResponse({ status: 200, description: 'List of jobs with the specified name' })
  getListJobsFromName(@Param('name') name: string, @Res() res) {
      return this.jobService.getListJobsFromName(name, res);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/upload-img-job/:id')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload image for a specific job by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the job' })
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Image file to upload for the job',
    type: 'multipart/form-data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
          description: 'File to upload',
        },
      },
    },
  })
  @ApiResponse({ status: 201, description: 'Image uploaded successfully for the job.' })
  upLoadImgJob(@UploadedFile() file, @Param('id') id: string, @Req() req, @Res() res) {
      const userId = req.user.userId;
      return this.jobService.uploadImgJob(+id, userId, res, file);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/get-job-paginated')
  @ApiOperation({ summary: 'Get paginated jobs' })
  @ApiQuery({ name: 'paginationDto', type: PaginationDto, description: 'Pagination data' })
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'List of paginated jobs' })
  getPaginatedJobs(@Query() paginationDto: PaginationDto, @Res() res) {
      return this.jobService.getPaginatedJobs(paginationDto, res);
  }
}
