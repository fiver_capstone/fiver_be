import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { PrismaClient } from '@prisma/client';
import { failCode, successCode } from 'src/configs/response';
import { Response } from 'express';
import { uploadToCloudinary } from 'src/utils/uploadToCloudinary';
import { validate } from 'class-validator';
import { PaginationDto } from './dto/paginated-job.dto';
@Injectable()
export class JobService {
  prisma = new PrismaClient()
  async createNewJob(body: CreateJobDto, userId: number, file: Express.Multer.File, res: Response) {
    const numericFields = ['rating', 'price', 'star_job', 'type_job_id'];
    for (const field of numericFields) {
      body[field] = +body[field];
    }
    const jobDto = Object.assign(new CreateJobDto(), body);
    const errors = await validate(jobDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    if (!userId) {
      failCode(res, "", "User not found");
      return;
    }

    if (!file || !file.buffer) {
      failCode(res, "", "No file uploaded");
      return;
    }

    const cloudinaryResult: any = await uploadToCloudinary(file);

    const createdNewJob = await this.prisma.job.create({
      data: { ...body, image: cloudinaryResult.url, user_id: userId }
    });


    if (createdNewJob) {
      successCode(res, createdNewJob, "Success Added Job");
    } else {
      failCode(res, "", "Fail to add");
    }
  }
  async getAllJob(res: Response) {
    const data = await this.prisma.job.findMany({})
    if (data) {
      successCode(res, data, "Success Loaded All Jobs")
    } else {
      failCode(res, "", "Not found")
    }

  }

  async getOneJob(id: number, res: Response) {
    const data = await this.prisma.job.findFirst({
      where: {
        id,
      },
      include: {
        user: {
          select: {
            name: true,
            avatar: true
          }
        },
        detail_type_job: {
          include: {
            type_job: true
          }
        }
      }
      
    })
    if (data) {
      successCode(res, data, "Success Loaded All Jobs")
    } else {
      failCode(res, "", "Not found")
    }

  }

  async updateJob(id: number, body: UpdateJobDto, file: Express.Multer.File, userId: number, res: Response) {
    const numericFields = ['rating', 'price', 'star_job', 'type_job_id'];
    for (const field of numericFields) {
      if (body[field] !== undefined && body[field] !== null) {
        body[field] = +body[field];
      }
    }
    const jobDto = Object.assign(new UpdateJobDto(), body);
    const errors = await validate(jobDto, { skipMissingProperties: true });
    if (errors.length > 0) {
      throw new BadRequestException("Please fill in the field want to update");
    }

    const data = await this.prisma.job.findMany({
      where: {
        id,
        user_id: userId,
      }
    })
    if (data.length === 0) {
      return failCode(res, "", "Not found");
    }
    const updateData: UpdateJobDto = { ...body };
    if (file) {
      const cloudinaryResult = await uploadToCloudinary(file);
      if (cloudinaryResult) {
        updateData.image = cloudinaryResult.url;
      }
    }
    try {
      const updated = await this.prisma.job.update({
        where: {
          id,
          user_id: userId
        },
        data: updateData
      })

      return successCode(res, updated, "Successfully updated");
    } catch (error) {
      console.error("Update error:", error);
      return failCode(res, "", "Fail to update");
    }
  }

  async deleteJob(id: number, userId: number, res: Response) {
    const data = await this.prisma.job.findFirst({
      where: {
        id,
        user_id: userId
      }
    })
    if (data) {
      const deleted = await this.prisma.job.delete({
        where: {
          id: data.id
        }
      })
      if (deleted) {
        successCode(res, deleted, "Sucessfully Deleted")
      }
    } else {
      failCode(res, "", "Not found")
    }
    return `This action removes a #${id} job`;
  }
  async getMenu(res: Response) {
    const data = await this.prisma.type_job.findMany({
      include: {
        detail_type_job: {
          include: {
            job: true
          }
        }
      }
    })
    if (data) {
      successCode(res, data, "Success Loaded Menu")
    } else {
      failCode(res, "", "Fail to load Menu")
    }
  }
  async getDetailTypeJobToTypeJob(id: number, res: Response) {
    const data = await this.prisma.type_job.findMany({
      where: {
        id
      },
      include: {
        detail_type_job: true
      }
    })
    if (data) {
      successCode(res, data, "Success Loaded Detail Type Job")
    } else {
      failCode(res, "", "Fail to load")
    }
  }
  async getJobToDetailTypeJob(id: number, res: Response) {
    const data = await this.prisma.detail_type_job.findMany({
      where: {
        id: id
      },
      include: {
        job: {
          include: {
            user: {
              select: {
                name: true,
                avatar: true
              }
            }
          }
        }
      }
    });
    if (data) {
      successCode(res, data, "Success Loaded Detail Type Job")
    } else {
      failCode(res, "", "Fail to load")
    }
  }
  async getDetailTypeJob(id: number, res: Response) {
    const data = await this.prisma.detail_type_job.findMany({
      where: {
        id,
      },
      include: {
        type_job: true
      }
    })
    if (data) {
      successCode(res, data, "Success Loaded Detail Type Job")
    } else {
      failCode(res, "", "Fail to load")
    }
  }
  async getDetailJob(id: number, res: Response) {
    const data = await this.prisma.job.findMany({
      where: {
        id,
      },
      include: {
        detail_type_job: true
      }
    })
    if (data) {
      successCode(res, data, "Success Loaded Detail Type Job")
    } else {
      failCode(res, "", "Fail to load")
    }
  }
  async getListJobsFromName(name: string, res: Response) {
    if (name) {
      const data = await this.prisma.job.findMany({
        where: {
          job_name: {
            contains: name,
          }
        }
      })
      if (data.length > 0) {
        successCode(res, data, "Success Loaded Detail Type Job")
      } else {
        failCode(res, "", "Fail to load")
      }
    }
  }
  async uploadImgJob(id: number, userId: number, res: Response, file: Express.Multer.File) {
    const findData = await this.prisma.job.findUnique({
      where: {
        id,
        user_id: userId
      }
    })
    if (findData) {
      if (!file || !file.buffer) {
        throw new BadRequestException('No file uploaded');
      }
      const cloudinaryResult: any = await uploadToCloudinary(file)
      const dbResult = await this.prisma.job.update({
        where: {
          id: findData.id
        },
        data: {
          image: cloudinaryResult.url
        }
      })
      if (dbResult) {
        successCode(res, dbResult, "Success Loaded Detail Type Job")
      } else {
        failCode(res, "", "Fail to load")
      }
    } else {
      failCode(res, "", "Not found or Unauthorized to Upload the Img")
    }
  }
  async getPaginatedJobs(paginationDto: PaginationDto, res: Response): Promise<any> {
    const { pageIndex, pageSize, keyword } = paginationDto;
    const skip = (pageIndex - 1) * pageSize;
    const take = +pageSize;
    let where = {};
    if (keyword) {
      where = {
        OR: [
          { job_name: { contains: keyword } },
        ],
      };
    }
    const result = await this.prisma.job.findMany({
      where,
      skip,
      take,
  });
   if(result){
    successCode(res,result,"Success loaded information")
  }else{
    failCode(res,"","Fail to loaded")
  }
  }

}
