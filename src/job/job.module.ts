import { Module } from '@nestjs/common';
import { JobService } from './job.service';
import { JobController } from './job.controller';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';
import { memoryStorage } from 'multer';
@Module({
  imports: [JwtModule.register({}), MulterModule.register({
    storage: memoryStorage()
  })],
  controllers: [JobController],
  providers: [JobService],
})
export class JobModule {}
