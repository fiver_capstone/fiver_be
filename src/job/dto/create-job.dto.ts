import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";


export class CreateJobDto {
    @IsNotEmpty()
    @IsString()
    job_name: string;
    @IsNotEmpty()
    @IsNumber()
    rating:number;
    @IsNotEmpty()
    @IsNumber()
    price:number;
    @IsNotEmpty()
    @IsString()
    description:string;
    @IsNotEmpty()
    @IsString()
    short_description:string;
    @IsNotEmpty()
    @IsNumber()
    star_job:number;
    @IsNotEmpty()
    @IsNumber()
    type_job_id:number;
}
