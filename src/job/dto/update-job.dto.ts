import { PartialType } from '@nestjs/mapped-types';
import { CreateJobDto } from './create-job.dto';
import { IsString,IsOptional,IsNumber } from 'class-validator';
export class UpdateJobDto  { 
    @IsString()
    @IsOptional()
    job_name?: string;
    @IsNumber()
    @IsOptional()
    rating?:number;
    @IsNumber()
    @IsOptional()
    price?:number;
    @IsString()
    @IsOptional()
    image?:string;
    @IsString()
    @IsOptional()
    description?:string;
    @IsNumber()
    @IsOptional()
    type_job_id?:number;
    @IsString()
    @IsOptional()
    short_description?:string;
    @IsNumber()
    @IsOptional()
    star_job?:number;
 
}


