import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Req,
  Res,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { user } from '@prisma/client';
import { HttpException } from '@nestjs/common/exceptions';
import { AuthGuard } from '@nestjs/passport/dist';
import { ResponseData } from 'src/global/globalClass';
import { HttpMessage, HttpStatus } from 'src/global/globalEnum';
import { FileInterceptor } from '@nestjs/platform-express';

@UseGuards(AuthGuard('jwt'))
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/create-user')
  @UseInterceptors(FileInterceptor('image'))
  async create(
    @Body()
    createUserDto: CreateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<ResponseData<CreateUserDto>> {
    try {
      const newUser: CreateUserDto = await this.userService.createUser(
        createUserDto,
        file,
      );

      return new ResponseData<CreateUserDto>(
        newUser,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/get-user')
  async getUser(
    @Body('page') page: number,
    @Body('limit') limit: number,
  ): Promise<ResponseData<user[]>> {
    try {
      if (!page) {
        page = 1;
      }

      if (!limit) {
        limit = 1000;
      } else {
        limit = Math.min(5, limit);
      }
      const users = await this.userService.getUser(page, limit);
      return new ResponseData<user[]>(
        users,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/get-user-by-id/:id')
  async getUserById(@Param('id') id: string): Promise<ResponseData<user>> {
    try {
      const user = await this.userService.getUserById(+id);
      return new ResponseData<user>(
        user,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch('update-user/:id')
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<ResponseData<UpdateUserDto>> {
    try {
      const userUpdate = await this.userService.updateUser(+id, updateUserDto);
      return new ResponseData<UpdateUserDto>(
        userUpdate,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete('delete-user/:id')
  async removeUser(@Param('id') id: string): Promise<ResponseData<user>> {
    try {
      await this.userService.removeUser(+id);
      return new ResponseData<user>(
        null,
        HttpStatus.SUCCESS,
        HttpMessage.SUCCESS,
      );
    } catch (error) {
      throw new HttpException(
        HttpMessage.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  @UseInterceptors(FileInterceptor('file'))
  @Post('upload-avatar')
  uploadAvatar(@UploadedFile() file, @Req() req, @Res() res){
    const userId = req.user.userId;
      return this.userService.uploadAvatar( userId, res, file);
  }
}
