import { BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaClient, user } from '@prisma/client';
import { HttpMessage } from 'src/global/globalEnum';
import { uploadToCloudinary } from 'src/utils/uploadToCloudinary';
import { failCode, successCode } from 'src/configs/response';
import { Response } from 'express';

@Injectable()
export class UserService {
  prisma = new PrismaClient();

  async createUser(
    createUserDto: CreateUserDto,
    file: Express.Multer.File,
  ): Promise<CreateUserDto> {
    const cloudinaryResult: any = await uploadToCloudinary(file);

    const newUser = await this.prisma.user.create({
      data: { ...createUserDto, avatar: cloudinaryResult.url },
    });
    return newUser;
  }
  async getUser(page: number, limit: number): Promise<user[]> {
    return await this.prisma.user.findMany({
      skip: (page - 1) * limit,
      take: limit,
    });
  }

  getUserById(id: number) {
    const user = this.prisma.user.findFirst({
      where: {
        id,
      },
    });
    return user;
  }

  updateUser(id: number, updateUserDto: UpdateUserDto) {
    const userUpdate = this.prisma.user.findFirst({
      where: {
        id,
      },
    });
    if (userUpdate) {
      const userUpdated = this.prisma.user.update({
        where: { id },
        data: updateUserDto,
      });
      return userUpdated;
    } else {
      throw new NotFoundException(HttpMessage.NOT_FOUND);
    }
  }

  async removeUser(id: number) {
    await this.prisma.user.delete({
      where: {
        id,
      },
    });
    return;
  }
  async uploadAvatar(userId: number, res:Response, file: Express.Multer.File){
    const findData = await this.prisma.user.findUnique({
      where: {
        id: userId
      }
    })
    if (findData) {
      console.log('file: ',file);
      if (!file || !file.buffer) {
        throw new BadRequestException('No file uploaded');
      }
      const cloudinaryResult: any = await uploadToCloudinary(file)
      const dbResult = await this.prisma.user.update({
        where: {
          id: findData.id
        },
        data: {
          avatar: cloudinaryResult.url
        }
      })
      if (dbResult) {
        successCode(res, dbResult, "Success Loaded Detail Type Job")
      } else {
        failCode(res, "", "Fail to load")
      }
    } else {
      failCode(res, "", "Not found or Unauthorized to Upload the Img")
    }
  }
}
