import { IsEmpty, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateUserDto {
  @IsEmpty()
  @IsNumber()
  id?: number;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  name: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  email: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  password: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  phone: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  birthday: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  gender: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  role: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  skill: string;
  @IsNotEmpty({ message: 'This field is required' })
  @IsString()
  certification: string;
  @IsEmpty()
  @IsNumber()
  avatar?: string;
}
