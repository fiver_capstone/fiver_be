import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class JwtStrategy extends
    PassportStrategy(Strategy) {
    // ktra token
    
    constructor(config: ConfigService) {
        console.log('Retrieved KEY:', config.get("KEY"));
        super({
            jwtFromRequest:
                ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.get("KEY"),
            

        });
    }
    // tra ve du lieu khi 1 API goi thanh cong co chua token
    async validate(tokenDecode: any) {
    // check quyen
    const userId = tokenDecode.user_id; // assuming the token's payload has a user_id field
    // For instance, you might want to return a user object instead of just the token's payload
    return { userId: userId, ...tokenDecode }
    }
}