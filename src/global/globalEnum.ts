export enum HttpStatus {
  SUCCESS = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403, // The client does not have access rights to the content
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500,
}
export enum HttpMessage {
  SUCCESS = 'Successful responses',
  BAD_REQUEST = 'Bad Request',
  UNAUTHORIZED = 'Unauthorized',
  FORBIDDEN = 'Forbidden',
  NOT_FOUND = 'The server cannot find the requested resource',
  INTERNAL_SERVER_ERROR = 'Internal Server Error',
}
