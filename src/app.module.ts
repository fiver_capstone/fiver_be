import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { JwtStrategy } from './strategy/jwt.strategy';
import { CommentsModule } from './comments/comments.module';
import { DetailTypeJobModule } from './detail-type-job/detail-type-job.module';
import * as cloudinary from 'cloudinary';
import { JobModule } from './job/job.module';
import { JobTypeModule } from './job-type/job-type.module';
import { JobHireModule } from './job-hire/job-hire.module';
@Module({
  imports: [
    UserModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    AuthModule,
    CommentsModule,
    DetailTypeJobModule,
    JobModule,
    JobTypeModule,
    JobHireModule],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {
  constructor(private configService: ConfigService) {
    this.initializeCloudinary();
  }

  private initializeCloudinary(): void {
    cloudinary.v2.config({
      cloud_name: this.configService.get<string>('CLOUDINARY_NAME'),
      api_key: this.configService.get<string>('CLOUDINARY_KEY'),
      api_secret: this.configService.get<string>('CLOUDINARY_SECRET'),
    });
  }
}
