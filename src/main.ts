import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from "express";
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder, SwaggerDocumentOptions } from '@nestjs/swagger';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const option:SwaggerDocumentOptions = {
    include: [AppModule],
    deepScanRoutes: true
  }
  app.setGlobalPrefix('api');
  const config = new DocumentBuilder()
  .setTitle('API Title')
  // .setBasePath('api')
  .setDescription('API Description')
  .setVersion('1.0')
  .addBearerAuth() // This is essential for JWT authentication
  .build();

 
  app.enableCors()
  app.use(express.static(".")) //dinh vi lai duong dan de load tai nguyen
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
  }));
  const document = SwaggerModule.createDocument(app, config,option);
  SwaggerModule.setup('swagger', app, document); 
 
  await app.listen(8080);
}
bootstrap();
// main => module => controller(tao API) => service(logic)
// module => ket noi thanh phan controller vs service va module: usermodule, foodmodule cua cac doi tuong khac