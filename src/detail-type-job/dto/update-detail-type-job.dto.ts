
import { CreateDetailTypeJobDto } from './create-detail-type-job.dto';
import { TypeJobDto } from './create-detail-type-job.dto';
export class UpdateDetailTypeJobDto extends CreateDetailTypeJobDto{}
export class UpdateTypeJobDto extends TypeJobDto{}
