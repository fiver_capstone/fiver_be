import { IsNotEmpty } from 'class-validator';

export class CreateDetailTypeJobDto {
  @IsNotEmpty()
  detail_name: string;

  @IsNotEmpty()
  image: string;

  @IsNotEmpty()
  job_type_id: number;
}
export class TypeJobDto {
  @IsNotEmpty()
  name_job_type: string
}