export class PaginationDto {
    pageIndex: number;
    pageSize: number;
    keyword?: string;
}
