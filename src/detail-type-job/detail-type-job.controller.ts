import {
  ApiTags, ApiBearerAuth, ApiOperation,
  ApiResponse, ApiBody, ApiParam, ApiQuery
} from '@nestjs/swagger';
import {
  Controller, Get, Post, Body, Patch,
  Param, Delete, UseGuards, Res, Req,
  UseInterceptors, UploadedFile, Query
} from '@nestjs/common';
import { DetailTypeJobService } from './detail-type-job.service';
import {
  CreateDetailTypeJobDto, TypeJobDto
} from './dto/create-detail-type-job.dto';
import {
  UpdateDetailTypeJobDto, UpdateTypeJobDto
} from './dto/update-detail-type-job.dto';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { PaginationDto } from './dto/pagination-dto';

@ApiTags('DetailTypeJob')
@Controller('detail-type-job')
export class DetailTypeJobController {
  constructor(private readonly detailTypeJobService: DetailTypeJobService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('/post-detail-type-job')
  @ApiOperation({ summary: 'Create a new detail type job' })
  @ApiBearerAuth()
  @ApiBody({ type: CreateDetailTypeJobDto })
  @ApiResponse({ status: 201, description: 'Detail type job created.' })
  postDetailTypeJob(@Body() createDetailTypeJobDto: CreateDetailTypeJobDto, @Res() res, @Req() req) {
    const userId = req.user.userId;
    return this.detailTypeJobService.postDetailTypeJob(createDetailTypeJobDto, res, +userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/add-new-type-job')
  @ApiOperation({ summary: 'Add a new type job' })
  @ApiBearerAuth()
  @ApiBody({ type: TypeJobDto })
  @ApiResponse({ status: 201, description: 'New type job added.' })
  postNewTypeGroupJob(@Body() typeJobDto: TypeJobDto, @Req() req, @Res() res,) {
    const userId = req.user.userId;
    return this.detailTypeJobService.postNewTypeGroupJob(+userId, res, typeJobDto);
  }

  @Get('/get-all')
  @ApiOperation({ summary: 'Retrieve all detail jobs' })
  @ApiResponse({ status: 200, description: 'List of all detail jobs' })
  getAllDetailJob() {
    return this.detailTypeJobService.getAllDetailJob();
  }

  @Get('/get-detail/:id')
  @ApiOperation({ summary: 'Retrieve a specific detail job by ID' })
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the detail job' })
  @ApiResponse({ status: 200, description: 'Specific detail job data' })
  getSpecificDetailJob(@Param('id') id: string, @Res() res) {
    return this.detailTypeJobService.getSpecificDetailJob(+id, res);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('/edit/:id')
  @ApiOperation({ summary: 'Update a specific detail job by ID' })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the detail job' })
  @ApiBody({ type: UpdateDetailTypeJobDto })
  @ApiResponse({ status: 200, description: 'Detail job updated.' })
  updateSpecificDetailJob(@Param('id') id: string, @Body() updateDetailTypeJobDto: UpdateDetailTypeJobDto, @Res() res, @Req() req) {
    const userId = req.user.userId;
    return this.detailTypeJobService.updateSpecificDetailJob(+id, updateDetailTypeJobDto, res, +userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('/edit-type-job-group/:id')
  @ApiOperation({ summary: 'Update a type job group by ID' })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the type job group' })
  @ApiBody({ type: UpdateTypeJobDto })
  @ApiResponse({ status: 200, description: 'Type job group updated.' })
  updateTypeJobGroup(@Param('id') id: string, @Body() updateTypeJobDto: UpdateTypeJobDto, @Res() res, @Req() req) {
    const userId = req.user.userId;
    return this.detailTypeJobService.updateNewTypeGroupJob(+userId, res, updateTypeJobDto, +id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/delete/:id')
  @ApiOperation({ summary: 'Delete a detail job by ID' })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the detail job' })
  @ApiResponse({ status: 200, description: 'Detail job deleted.' })
  deleteDetailJob(@Param('id') id: string, @Res() res, @Req() req) {
    const userId = req.user.userId;
    return this.detailTypeJobService.deleteDetailJob(+id, +userId, res);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/upload-image-group/:id')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload image for a detail type job group' })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: 'number', description: 'ID of the detail type job group' })
  @ApiResponse({ status: 201, description: 'Image uploaded successfully.' })
  async uploadImage(@UploadedFile() file, @Param('id') id: string, @Req() req, @Res() res) {
    const userId = req.user.userId;
    return this.detailTypeJobService.updateImageDetails(file, +id, userId, res);
  }

  @Get('/paginated')
  @ApiOperation({ summary: 'Retrieve paginated detail type jobs' })
  @ApiQuery({ name: 'paginationDto', type: PaginationDto, description: 'Pagination data' })
  @ApiResponse({ status: 200, description: 'List of paginated detail type jobs' })
  getPaginatedJobs(@Query() paginationDto: PaginationDto, @Res() res) {
    return this.detailTypeJobService.getPaginatedJobs(paginationDto, res);
  }
}
