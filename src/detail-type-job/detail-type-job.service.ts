import { BadRequestException, Injectable} from '@nestjs/common';
import { CreateDetailTypeJobDto } from './dto/create-detail-type-job.dto';
import { UpdateDetailTypeJobDto } from './dto/update-detail-type-job.dto';
import { PrismaClient, detail_type_job } from '@prisma/client';
import { failCode, successCode } from 'src/configs/response';
import { Response } from 'express';
import { validate } from 'class-validator';
import { TypeJobDto } from './dto/create-detail-type-job.dto';
import * as cloudinary from 'cloudinary';
import { PaginationDto } from './dto/pagination-dto';
import { uploadToCloudinary } from 'src/utils/uploadToCloudinary';
@Injectable()
export class DetailTypeJobService {
  prisma = new PrismaClient()
 async postDetailTypeJob(createDetailTypeJobDto: CreateDetailTypeJobDto,res:Response,userId:number) {
  const user = await this.prisma.user.findUnique({
    where:{
      id: userId
    }
  });

  if (!user || user.role !== 'admin') { 
    failCode(res, "", "Unauthorized to upload");
    return;
  }
  if(createDetailTypeJobDto){
      const postJob = await this.prisma.detail_type_job.create({data:createDetailTypeJobDto})
      if(postJob){
        successCode(res,postJob,"Success Added new detail job")
      }
    }
  }
 async getAllDetailJob() {
    const data = await this.prisma.detail_type_job.findMany()
    return data;
  }
  async getSpecificDetailJob(id: number,res:Response) {
    const specificJob = await this.prisma.type_job.findUnique({
      where:{
        id
      },
      include:{detail_type_job:true}
    })
    if(specificJob){
      successCode(res,specificJob,"Success Loaded type job")
      return specificJob
    }else{
      failCode(res,"","Fail to load")
    }
    console.log(specificJob);
    return
  }
  async updateSpecificDetailJob(id: number, updateDetailTypeJobDto: UpdateDetailTypeJobDto, res: Response,userId:number) {
      const errors = await validate(updateDetailTypeJobDto);
      if (errors.length > 0) {
          const errorMessages = errors.map(error => ({
              property: error.property,
              constraints: error.constraints
          }));
          
          return res.status(400).json({
              message: "Input validation failed",
              errors: errorMessages
          });
      }
      const user = await this.prisma.user.findUnique({
        where:{
          id: userId
        }
      });
    
      if (!user || user.role !== 'admin') {
        failCode(res, "", "Unauthorized to edit");
        return;
      }
    
      if (updateDetailTypeJobDto) {
          const updatedData = await this.prisma.detail_type_job.update({
              where: { id },
              data: updateDetailTypeJobDto
          });
          
          if (updatedData) {
              successCode(res, updateDetailTypeJobDto, "Success Updated");
          } else {
              failCode(res, "", "Failed to update");
          }
      }
  }
  async deleteDetailJob(id: number, userId: number, res: Response) {
    const user = await this.prisma.user.findUnique({
      where:{
        id: userId
      }
    });
  
    if (!user || user.role !== 'admin') {
      failCode(res, "", "Unauthorized to deleted");
      return;
    }
  
    const validateId = await this.prisma.detail_type_job.findUnique({
      where: {
        id
      }
    });
  
    if (validateId) {
      const deleted = await this.prisma.detail_type_job.delete({
        where: { id: validateId.id }
      });
  
      if (deleted) {
        successCode(res, deleted, "Deleted comment");
      } else {
        failCode(res, "", "Fail to delete");
      }
    } else {
      failCode(res, "", "Not found");
    }
  }
  async postNewTypeGroupJob(userId:number,res:Response,typeJob:TypeJobDto){
    const user = await this.prisma.user.findUnique({
      where:{
        id: userId
      }
    })
    if(!user || user.role !== 'admin'){
      failCode(res, "", "Unauthorized to upload");
      return;
    }
    if(typeJob){
      const addedData = await this.prisma.type_job.create({
        data: typeJob
      })
      if(addedData){
        successCode(res,addedData,"Success Added new type job")
      }else{
        failCode(res,"","Fail To Add")
      }
    }
  }
  async updateNewTypeGroupJob(userId:number, res:Response, typeJob:TypeJobDto, id:number) {
    const user = await this.prisma.user.findUnique({
      where: {
        id: userId
      }
    });
    
    if (!user || user.role !== 'admin') {
      failCode(res, "", "Unauthorized to upload");
      return;
    }

    if (typeJob) {
      const existingTypeJob = await this.prisma.type_job.findUnique({
          where: {
              id
          }
      });

      if (!existingTypeJob) {
          failCode(res, "", "Not found");
          return;
      }
      try {
          const updateData = await this.prisma.type_job.update({
              where: {
                  id
              },
              data: typeJob
          });

          successCode(res, updateData, "Success Updated new type job");
      } catch (error) {
          failCode(res, "", "Fail To Update");
      }
    } else {
      failCode(res, "", "Invalid typeJob data");
    }
}
async updateImageDetails(file: Express.Multer.File, id: number,userId:number,res:Response): Promise<any> {
  const user = await this.prisma.user.findUnique({
    where:{
      id:userId
    }
  })
  if(user.role==="admin"){

    if (!file || !file.buffer) {
      throw new BadRequestException('No file uploaded');
    }
    const cloudinaryResult: any = await uploadToCloudinary(file)
  
    const dbResult = await this.prisma.detail_type_job.update({
      where: { id: id },
      data: {
        image: cloudinaryResult.url, 
      },
    });
  
    return {
      message: 'Image uploaded and updated successfully!',
      data: dbResult,
    };
  }else{
    failCode(res,"","Sorry you don't have permission for update image")
  }
}

async getPaginatedJobs(paginationDto: PaginationDto,res:Response): Promise<any> {
  const { pageIndex, pageSize, keyword } = paginationDto;

  const skip = (pageIndex - 1) * pageSize;
  const take = +pageSize;

  let where = {};
  if (keyword) {
      where = {
          OR: [
              { detail_name: { contains: keyword } },
          ],
      };
  }
  
  const result = await this.prisma.detail_type_job.findMany({
      where,
      skip,
      take,
  });
  if(result){
    successCode(res,result,"Success loaded information")
  }else{
    failCode(res,"","Fail to loaded")
  }
}
}
