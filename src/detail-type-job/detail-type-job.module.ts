import { Module } from '@nestjs/common';
import { DetailTypeJobService } from './detail-type-job.service';
import { DetailTypeJobController } from './detail-type-job.controller';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';
import { memoryStorage } from 'multer';
import * as cloudinary from 'cloudinary';
import { config } from 'dotenv';
// config();  
// cloudinary.v2.config({
//   cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
//   api_key: process.env.CLOUDINARY_API_KEY,
//   api_secret: process.env.CLOUDINARY_API_SECRET,
// });
@Module({
  imports: [JwtModule.register({}), MulterModule.register({
    storage: memoryStorage()
  })],
  controllers: [DetailTypeJobController],
  providers: [DetailTypeJobService],
})
export class DetailTypeJobModule {}
