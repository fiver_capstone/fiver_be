import { Response } from 'express';

const successCode = (res: Response, data: any, message: string) => {
  res.status(200).json({
    message,
    content: data,
  });
};

const failCode = (res: Response, data: any, message: string) => {
  res.status(400).json({
    message,
    content: data,
  });
};

const errorCode = (res: Response, message: string) => {
  res.status(500).json({
    message,
  });
};

export { successCode, failCode, errorCode };
