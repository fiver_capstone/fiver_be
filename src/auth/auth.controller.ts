import { Controller, Post, Body, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { ApiTags, ApiOperation, ApiBody, ApiResponse } from '@nestjs/swagger';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("/login")
  @ApiOperation({ summary: 'Log in with email and password' })
  @ApiBody({
    description: 'User login data',
    schema: {
      type: 'object',
      properties: {
        email: { type: 'string' },
        password: { type: 'string' },
      },
      required:["email","password"]
    },
  })
  @ApiResponse({ status: 200, description: 'Logged in successfully' })
  @ApiResponse({ status: 401, description: 'Invalid credentials' })
  login(@Body() body, @Res() res: Response){
    return this.authService.login(body, res);
  }

  @Post("/sign-up")
  @ApiOperation({ summary: 'Sign up for a new account' })
  @ApiBody({
    description: 'User sign-up data',
    schema: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        email: { type: 'string' },
        password: { type: 'string' },
        phone: { type: 'string' },
        birthday: { type: 'string' },
        gender: { type: 'string' },
        role: { type: 'string' },
        skill: { type: 'array', items: { type: 'string' } },
        certificate: { type: 'array', items: { type: 'string' } },
      },
      required: ['name', 'email', 'password', 'phone', 'birthday', 'gender', 'role']
    },
  })
  
  @ApiResponse({ status: 201, description: 'Account created successfully' })
  @ApiResponse({ status: 400, description: 'Bad request, possible duplicate email or invalid data' })
  signUp(@Body() body, @Res() res: Response){
    return this.authService.signUp(body, res);
  }
}
