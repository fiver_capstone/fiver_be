import { Injectable } from '@nestjs/common';
import { PrismaClient, user } from '@prisma/client';
import { failCode, successCode } from 'src/configs/response';
import * as bcrypt from "bcrypt";
import { Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService
    ){}
  prisma = new PrismaClient()
  async login({email,password},res:Response) {
    let checkEmailUser = await this.prisma.user.findFirst({
      where: {
        email,
      }
    })

    if(checkEmailUser){
      if(bcrypt.compareSync(password, checkEmailUser.password)){
        checkEmailUser = { ...checkEmailUser, password: "" };
        const token = await this.jwtService.signAsync(
          { user_id: checkEmailUser.id }, 
          { secret: this.configService.get("KEY"), expiresIn: "24h" }
        );
        successCode(
          res,
          { user_id: checkEmailUser.id,token,},
          "Login successfully"
        );
      }else{
        failCode(res,"","Password incorrect")
      }
    }else{
      failCode(res,"","Email incorrect")
    }
  }
 async signUp(userSignUp:user,res:Response) {
  const  email  = userSignUp.email;
   const existedUser = await this.prisma.user.findFirst({
    where:{
      email,
    }
   })
   
   if(existedUser){
    failCode(res,"","Username existed")
   }else{
    const newUser = {...userSignUp,password: bcrypt.hashSync(userSignUp.password, 10),}
    let addedUser = await this.prisma.user.create({
      data:newUser
    })
    addedUser = {...addedUser,password:""}
    return successCode(res,addedUser,"Successful created user")
   }
  }
}
