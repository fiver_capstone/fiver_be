
FROM node:16

WORKDIR /usr/src/app

COPY package*.json .

RUN yarn install --legacy-peer-deps

COPY prisma ./prisma/

RUN yarn prisma generate

COPY . .

RUN yarn build

EXPOSE 8080
# npm start || node index.js =>khoi chay server
CMD ["node","dist/main.js"]
