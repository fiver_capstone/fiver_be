/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `date_comment` datetime DEFAULT NULL,
  `content` varchar(300) DEFAULT NULL,
  `star_comment` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `detail_type_job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `detail_name` varchar(150) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `job_type_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_type_id` (`job_type_id`),
  CONSTRAINT `detail_type_job_ibfk_1` FOREIGN KEY (`job_type_id`) REFERENCES `type_job` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hire_job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `date_hire` datetime DEFAULT NULL,
  `completed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `hire_job_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `hire_job_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_name` varchar(200) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `price` int DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `short_description` varchar(150) DEFAULT NULL,
  `star_job` int DEFAULT NULL,
  `type_job_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_job_id` (`type_job_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `job_ibfk_1` FOREIGN KEY (`type_job_id`) REFERENCES `detail_type_job` (`id`),
  CONSTRAINT `job_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `type_job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name_job_type` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `role` varchar(150) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `certification` varchar(200) DEFAULT NULL,
  `avatar` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `comment` (`id`, `job_id`, `user_id`, `date_comment`, `content`, `star_comment`) VALUES
(1, 1, 1, '2023-08-07 16:27:42', 'I like the way he designed the logo!', 5);
INSERT INTO `comment` (`id`, `job_id`, `user_id`, `date_comment`, `content`, `star_comment`) VALUES
(2, 2, 2, '2023-08-07 16:27:42', 'The flyer design is outstanding!', 4);
INSERT INTO `comment` (`id`, `job_id`, `user_id`, `date_comment`, `content`, `star_comment`) VALUES
(3, 3, 3, '2023-08-07 16:27:42', 'Great job on the front end development!', 3);
INSERT INTO `comment` (`id`, `job_id`, `user_id`, `date_comment`, `content`, `star_comment`) VALUES
(4, 4, 4, '2023-08-07 16:27:42', 'The mobile app works flawlessly!', 4),
(5, 5, 5, '2023-08-07 16:27:42', 'The UX research was incredibly insightful!', 5),
(6, 6, 6, '2023-08-07 16:27:42', 'The SEO analysis helped improve our website ranking!', 3),
(7, 7, 7, '2023-08-07 16:27:42', 'The blog content was engaging and well-written!', 4),
(8, 8, 8, '2023-08-07 16:27:42', 'The product video looks professional!', 5),
(9, 9, 9, '2023-08-07 16:27:42', 'The 3D characters are amazing!', 4),
(10, 10, 10, '2023-08-07 16:27:42', 'Our social media presence has improved!', 3),
(11, 6, 11, '2023-08-13 20:12:22', 'Such a great art', 5),
(12, 2, 2, '2023-08-14 21:38:41', 'Wonderful pieces of art', 4),
(13, 2, 4, '2023-08-14 21:48:41', 'Good works!', 3);

INSERT INTO `detail_type_job` (`id`, `detail_name`, `image`, `job_type_id`) VALUES
(1, 'Brand Logo Design', 'http://res.cloudinary.com/dd84mw590/image/upload/v1692130262/capstone_fiver/gjGoldLogo.svg', 1);
INSERT INTO `detail_type_job` (`id`, `detail_name`, `image`, `job_type_id`) VALUES
(2, 'Corporate Graphic Design', 'https://example.com/detail2.jpg', 2);
INSERT INTO `detail_type_job` (`id`, `detail_name`, `image`, `job_type_id`) VALUES
(3, 'Front End Development', 'https://example.com/detail3.jpg', 3);
INSERT INTO `detail_type_job` (`id`, `detail_name`, `image`, `job_type_id`) VALUES
(4, 'Back End Development', 'https://example.com/detail4.jpg', 3),
(5, 'UX Research', 'https://example.com/detail5.jpg', 5),
(6, 'SEO Analysis', 'https://example.com/detail6.jpg', 7),
(7, 'Blog Content Writing', 'https://example.com/detail7.jpg', 8),
(8, 'Product Video Editing', 'https://example.com/detail8.jpg', 9),
(9, '3D Character Modelling', 'https://example.com/detail9.jpg', 10),
(10, 'Social Media Marketing', 'https://example.com/detail10.jpg', 6),
(11, 'Marketing C to C', 'need to develop', 6),
(16, 'Mobile Application Design', 'http://res.cloudinary.com/dd84mw590/image/upload/v1692130126/capstone_fiver/model%20of%20the%20week.png', 2),
(17, 'Marketing B2B', 'still need to develop', 6),
(18, 'Marketing C2C', 'still need to develop', 10);

INSERT INTO `hire_job` (`id`, `job_id`, `user_id`, `date_hire`, `completed`) VALUES
(1, 1, 1, '2023-08-01 12:00:00', 1);
INSERT INTO `hire_job` (`id`, `job_id`, `user_id`, `date_hire`, `completed`) VALUES
(2, 2, 2, '2023-08-02 14:00:00', 0);
INSERT INTO `hire_job` (`id`, `job_id`, `user_id`, `date_hire`, `completed`) VALUES
(3, 3, 3, '2023-08-03 09:30:00', 1);
INSERT INTO `hire_job` (`id`, `job_id`, `user_id`, `date_hire`, `completed`) VALUES
(4, 4, 4, '2023-08-04 16:00:00', 0),
(5, 5, 5, '2023-08-05 10:15:00', 1),
(6, 6, 6, '2023-08-06 13:45:00', 0),
(7, 7, 7, '2023-08-07 11:00:00', 1),
(8, 8, 8, '2023-08-08 15:30:00', 0),
(9, 9, 9, '2023-08-09 12:15:00', 1),
(10, 10, 10, '2023-08-10 14:45:00', 0);

INSERT INTO `job` (`id`, `job_name`, `rating`, `price`, `image`, `description`, `short_description`, `star_job`, `type_job_id`, `user_id`) VALUES
(1, 'Brand Logo for XYZ', 5, 100, 'https://example.com/job1.jpg', 'Design a brand logo for XYZ company', 'Brand Logo Design', 5, 1, 1);
INSERT INTO `job` (`id`, `job_name`, `rating`, `price`, `image`, `description`, `short_description`, `star_job`, `type_job_id`, `user_id`) VALUES
(2, 'Corporate Flyer Design', 4, 200, 'https://example.com/job2.jpg', 'Design corporate flyers for ABC event', 'Corporate Graphic Design', 4, 2, 2);
INSERT INTO `job` (`id`, `job_name`, `rating`, `price`, `image`, `description`, `short_description`, `star_job`, `type_job_id`, `user_id`) VALUES
(3, 'Front End for Website', 3, 300, 'https://example.com/job3.jpg', 'Develop the front end for a website using React', 'Front End Development', 3, 3, 3);
INSERT INTO `job` (`id`, `job_name`, `rating`, `price`, `image`, `description`, `short_description`, `star_job`, `type_job_id`, `user_id`) VALUES
(4, 'Mobile App Development', 4, 800, 'https://example.com/job4.jpg', 'Develop a mobile app for iOS and Android', 'Mobile Development', 4, 4, 4),
(5, 'UX Research for New Product', 5, 150, 'https://example.com/job5.jpg', 'Conduct UX research for a new digital product', 'UX Research', 5, 5, 5),
(6, 'SEO Analysis for E-Commerce', 3, 120, 'https://example.com/job6.jpg', 'Perform an SEO analysis for an e-commerce website', 'SEO Analysis', 3, 6, 6),
(7, 'Blog Content Writing', 4, 60, 'https://example.com/job7.jpg', 'Write blog content for a technology website', 'Blog Content Writing', 4, 7, 7),
(8, 'Product Video Editing', 5, 250, 'https://example.com/job8.jpg', 'Edit a product video for a marketing campaign', 'Product Video Editing', 5, 8, 8),
(9, '3D Character Modelling', 4, 400, 'https://example.com/job9.jpg', 'Create 3D character models for a video game', '3D Character Modelling', 4, 9, 9),
(10, 'Social Media Marketing', 3, 200, 'https://example.com/job10.jpg', 'Manage social media marketing for a small business', 'Social Media Marketing', 3, 10, 10),
(11, 'Brand Logo for ABC', 5, 100, 'https://example.com/job1.jpg', 'Design a brand logo for ABC company', 'Brand Logo Design for ABC', 5, 1, 1),
(12, 'Graphic Design For Web for DEF', 4, 100, 'https://example.com/job1.jpg', 'Design for DEF company', 'Graphic Design for DEF', 4, 1, 1),
(13, 'Front End for Website A', 3, 300, 'https://example.com/job3.jpg', 'Develop the front end for Website A using React', 'Front End Development for A', 3, 3, 3),
(14, 'Back End for Website B', 3, 300, 'https://example.com/job3.jpg', 'Develop the back end for Website B using Node.js', 'Back End Development for B', 3, 4, 11),
(15, 'Full-stack for Website C', 3, 300, 'https://example.com/job3.jpg', 'Develop the full stack for Website C using MERN', 'Full Stack Development for C', 3, 3, 3),
(16, 'Marketing Video for Business and Sales', 4, 150, 'http://res.cloudinary.com/dd84mw590/image/upload/v1692370004/capstone_fiver/roman-protsyshyn-sONokkSBDZA-unsplash.jpg', 'Are you looking for an engaging Sales and Marketing video for digital OR physical products? Well, you\'ve arrived in the right spot. \nCheck out the following packages and choose wisely according to your requirement. And If you are looking for a customized marketing video then I would highly suggest to contact me and discuss it before placing an order.\n\n', 'I will create an animated marketing video for business and sales', 3, 6, 11),
(17, '3D product animation visualization video promo and mockup', 4, 150, 'http://res.cloudinary.com/dd84mw590/image/upload/v1692370004/capstone_fiver/roman-protsyshyn-sONokkSBDZA-unsplash.jpg', 'Welcome to my 3D product animation service\n\n\n\nA 3D Product visualization video can create your thinking real. Product Rendering is one of the smart things to show your product globally. I\'m a professional 3D artist. I\'ll help you to design a 3d model and a unique promo video with an eye-catching simulation of your product features. If your product is an industrial product, I\'ll create an explainer video with industrial product animation. \n\n I\'ll let you know all the updates from 3d modeling to 3d animation rendering. I\'ll appreciate you cooperating with me for a better result. Buyer satisfaction is my happiness. I\'ll work on the project until you\'re happy.\n\n\n\nWhat I need from you\n\nProduct image or sketch\nProduct logo and level ( If you have one)\nStoryboard ( If you have one)\nExample or reference video \n\n\nWhat my responsibility do for you\n\nQuick response all the time ( even after finishing the project )\nHigh-Quality HD resolution video (2k/4k)\nProduct mockup image from the different angel (2k/4k)\nAll the project files and 3D model ( fbx. obj. 3ds max file etc)\n\n\nIf you have any questions, feel free to ask me, please.', 'I will do 3d product animation visualization video promo and mockup', 3, 9, 11),
(25, '3D product animation visualization video promo and mockup', 4, 150, 'http://res.cloudinary.com/dd84mw590/image/upload/v1692391312/capstone_fiver/imgLogo-1692391311746-62331295.png', 'Welcome to my 3D product animation service\n\n\n\nA 3D Product visualization video can create your thinking real. Product Rendering is one of the smart things to show your product globally. I\'m a professional 3D artist. I\'ll help you to design a 3d model and a unique promo video with an eye-catching simulation of your product features. If your product is an industrial product, I\'ll create an explainer video with industrial product animation. \n\n I\'ll let you know all the updates from 3d modeling to 3d animation rendering. I\'ll appreciate you cooperating with me for a better result. Buyer satisfaction is my happiness. I\'ll work on the project until you\'re happy.\n\n\n\nWhat I need from you\n\nProduct image or sketch\nProduct logo and level ( If you have one)\nStoryboard ( If you have one)\nExample or reference video \n\n\nWhat my responsibility do for you\n\nQuick response all the time ( even after finishing the project )\nHigh-Quality HD resolution video (2k/4k)\nProduct mockup image from the different angel (2k/4k)\nAll the project files and 3D model ( fbx. obj. 3ds max file etc)\n\n\nIf you have any questions, feel free to ask me, please.', 'I will do 3d product animation visualization video promo and mockup', 3, 9, NULL),
(26, '3D product animation visualization video promo and mockup', 4, 200, 'http://res.cloudinary.com/dd84mw590/image/upload/v1692460111/capstone_fiver/marc-linnemann-DA1eGglMmlg-unsplash-1692460109547-518011111.jpg', 'Welcome to my 3D product animation service\n\n\n\nA 3D Product visualization video can create your thinking real. Product Rendering is one of the smart things to show your product globally. I\'m a professional 3D artist. I\'ll help you to design a 3d model and a unique promo video with an eye-catching simulation of your product features. If your product is an industrial product, I\'ll create an explainer video with industrial product animation. \n\n I\'ll let you know all the updates from 3d modeling to 3d animation rendering. I\'ll appreciate you cooperating with me for a better result. Buyer satisfaction is my happiness. I\'ll work on the project until you\'re happy.\n\n\n\nWhat I need from you\n\nProduct image or sketch\nProduct logo and level ( If you have one)\nStoryboard ( If you have one)\nExample or reference video \n\n\nWhat my responsibility do for you\n\nQuick response all the time ( even after finishing the project )\nHigh-Quality HD resolution video (2k/4k)\nProduct mockup image from the different angel (2k/4k)\nAll the project files and 3D model ( fbx. obj. 3ds max file etc)\n\n\nIf you have any questions, feel free to ask me, please.', 'I will do 3d product animation visualization video promo and mockup', 3, 9, 11);

INSERT INTO `type_job` (`id`, `name_job_type`) VALUES
(1, 'Logo Design');
INSERT INTO `type_job` (`id`, `name_job_type`) VALUES
(2, 'Graphic Design');
INSERT INTO `type_job` (`id`, `name_job_type`) VALUES
(3, 'Web Development');
INSERT INTO `type_job` (`id`, `name_job_type`) VALUES
(4, 'Mobile Development'),
(5, 'UX/UI Design'),
(6, 'Digital Marketing'),
(7, 'SEO Specialist'),
(8, 'Content Writing'),
(9, 'Video Editing and Animation'),
(10, '3D Modelling'),
(11, 'Music and Audio'),
(12, 'Music and Audio');

INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `birthday`, `gender`, `role`, `skill`, `certification`, `avatar`) VALUES
(1, 'John Doe', 'john.doe@gmail.com', 'password1', '1234567890', '1990-01-01', 'Male', 'Designer', 'Logo Design', 'Cert 1', 'https://fakeimg.com/avatar/1.jpg');
INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `birthday`, `gender`, `role`, `skill`, `certification`, `avatar`) VALUES
(2, 'Jane Smith', 'jane.smith@gmail.com', 'password2', '1234567891', '1991-01-01', 'Female', 'Developer', 'Web Development', 'Cert 2', 'https://fakeimg.com/avatar/2.jpg');
INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `birthday`, `gender`, `role`, `skill`, `certification`, `avatar`) VALUES
(3, 'Alice Johnson', 'alice.johnson@gmail.com', 'password3', '1234567892', '1992-01-01', 'Female', 'Writer', 'Content Writing', 'Cert 3', 'https://fakeimg.com/avatar/3.jpg');
INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone`, `birthday`, `gender`, `role`, `skill`, `certification`, `avatar`) VALUES
(4, 'Bob Miller', 'bob.miller@gmail.com', 'password4', '1234567893', '1993-01-01', 'Male', 'SEO Specialist', 'SEO', 'Cert 4', 'https://fakeimg.com/avatar/4.jpg'),
(5, 'Charlie Brown', 'charlie.brown@gmail.com', 'password5', '1234567894', '1994-01-01', 'Male', '3D Artist', '3D Modelling', 'Cert 5', 'https://fakeimg.com/avatar/5.jpg'),
(6, 'Diana Adams', 'diana.adams@gmail.com', 'password6', '1234567895', '1995-01-01', 'Female', 'Graphic Designer', 'Graphic Design', 'Cert 6', 'https://fakeimg.com/avatar/6.jpg'),
(7, 'Ethan Lee', 'ethan.lee@gmail.com', 'password7', '1234567896', '1996-01-01', 'Male', 'Mobile Developer', 'Mobile Development', 'Cert 7', 'https://fakeimg.com/avatar/7.jpg'),
(8, 'Fiona Clark', 'fiona.clark@gmail.com', 'password8', '1234567897', '1997-01-01', 'Female', 'UX/UI Designer', 'UX/UI Design', 'Cert 8', 'https://fakeimg.com/avatar/8.jpg'),
(9, 'George White', 'george.white@gmail.com', 'password9', '1234567898', '1998-01-01', 'Male', 'Digital Marketer', 'Digital Marketing', 'Cert 9', 'https://fakeimg.com/avatar/9.jpg'),
(10, 'Hannah Taylor', 'hannah.taylor@gmail.com', 'password10', '1234567899', '1999-01-01', 'Female', 'Video Editor', 'Video Editing', 'Cert 10', 'https://fakeimg.com/avatar/10.jpg'),
(11, 'Tommy Hill', 'tommyhill@gmail.com', '$2b$10$FZzwfgVWvLltz8ETkF3h/./xKtIq/8XE9/UIVcVALz7U1VHYd.TVG', '0477852412', '1988-08-29', 'Male', 'admin', 'Graphic Design', NULL, 'https://fakeimg.com/avatar/11.jpg');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;